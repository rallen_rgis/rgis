USE [Salesforce_ODS]
GO

/****** Object:  View [dbo].[vw_User]    Script Date: 24/11/2017 14:21:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW  [dbo].[vw_User] AS
SELECT  U.[Id]
      ,[Username]
      ,[LastName]
      ,[FirstName]
      ,[Name]
      ,[CompanyName]
      ,[Division]
      ,[Department]
      ,[Title]
      ,[Street]
      ,[City]
      ,[State]
      ,[PostalCode]
      ,CASE WHEN uc.country IS NULL then U.[Country] ELSE UC.country END As Country
      ,[Latitude]
      ,[Longitude]
      ,[GeocodeAccuracy]
      ,[Address]
      ,[Email]
      ,[EmailPreferencesAutoBcc]
      ,[EmailPreferencesAutoBccStayInTouch]
      ,[EmailPreferencesStayInTouchReminder]
      ,[SenderEmail]
      ,[SenderName]
      ,[Signature]
      ,[StayInTouchSubject]
      ,[StayInTouchSignature]
      ,[StayInTouchNote]
      ,[Phone]
      ,[Fax]
      ,[MobilePhone]
      ,[Alias]
      ,[CommunityNickname]
      ,[BadgeText]
      ,[IsActive]
      ,[TimeZoneSidKey]
      ,[UserRoleId]
	  ,CASE Name WHEN 'Winston Steadford' THEN 'Key Account Manager - UK' ELSE
	  (Select Name from SF_User_Role R where R.Id = UserRoleId) END as UserRoleName
      ,[LocaleSidKey]
      ,[ReceivesInfoEmails]
      ,[ReceivesAdminInfoEmails]
      ,[EmailEncodingKey]
      ,[ProfileId]
      ,[UserType]
      ,[LanguageLocaleKey]
      ,[EmployeeNumber]
      ,[DelegatedApproverId]
      ,[ManagerId]
      ,[LastLoginDate]
      ,[CreatedDate]
      ,[CreatedById]
      ,[LastModifiedDate]
      ,[LastModifiedById]
      ,[SystemModstamp]
      ,[OfflineTrialExpirationDate]
      ,[OfflinePdaTrialExpirationDate]
      ,[UserPermissionsMarketingUser]
      ,[UserPermissionsOfflineUser]
      ,[UserPermissionsAvantgoUser]
      ,[UserPermissionsCallCenterAutoLogin]
      ,[UserPermissionsMobileUser]
      ,[UserPermissionsSFContentUser]
      ,[UserPermissionsKnowledgeUser]
      ,[UserPermissionsInteractionUser]
      ,[UserPermissionsSupportUser]
      ,[UserPermissionsJigsawProspectingUser]
      ,[UserPermissionsChatterAnswersUser]
      ,[UserPermissionsWorkDotComUserFeature]
      ,[ForecastEnabled]
      ,[UserPreferencesActivityRemindersPopup]
      ,[UserPreferencesEventRemindersCheckboxDefault]
      ,[UserPreferencesTaskRemindersCheckboxDefault]
      ,[UserPreferencesReminderSoundOff]
      ,[UserPreferencesDisableAllFeedsEmail]
      ,[UserPreferencesDisableFollowersEmail]
      ,[UserPreferencesDisableProfilePostEmail]
      ,[UserPreferencesDisableChangeCommentEmail]
      ,[UserPreferencesDisableLaterCommentEmail]
      ,[UserPreferencesDisProfPostCommentEmail]
      ,[UserPreferencesApexPagesDeveloperMode]
      ,[UserPreferencesHideCSNGetChatterMobileTask]
      ,[UserPreferencesDisableMentionsPostEmail]
      ,[UserPreferencesDisMentionsCommentEmail]
      ,[UserPreferencesHideCSNDesktopTask]
      ,[UserPreferencesHideChatterOnboardingSplash]
      ,[UserPreferencesHideSecondChatterOnboardingSplash]
      ,[UserPreferencesDisCommentAfterLikeEmail]
      ,[UserPreferencesDisableLikeEmail]
      ,[UserPreferencesSortFeedByComment]
      ,[UserPreferencesDisableMessageEmail]
      ,[UserPreferencesJigsawListUser]
      ,[UserPreferencesDisableBookmarkEmail]
      ,[UserPreferencesDisableSharePostEmail]
      ,[UserPreferencesEnableAutoSubForFeeds]
      ,[UserPreferencesDisableFileShareNotificationsForApi]
      ,[UserPreferencesShowTitleToExternalUsers]
      ,[UserPreferencesShowManagerToExternalUsers]
      ,[UserPreferencesShowEmailToExternalUsers]
      ,[UserPreferencesShowWorkPhoneToExternalUsers]
      ,[UserPreferencesShowMobilePhoneToExternalUsers]
      ,[UserPreferencesShowFaxToExternalUsers]
      ,[UserPreferencesShowStreetAddressToExternalUsers]
      ,[UserPreferencesShowCityToExternalUsers]
      ,[UserPreferencesShowStateToExternalUsers]
      ,[UserPreferencesShowPostalCodeToExternalUsers]
      ,[UserPreferencesShowCountryToExternalUsers]
      ,[UserPreferencesShowProfilePicToGuestUsers]
      ,[UserPreferencesShowTitleToGuestUsers]
      ,[UserPreferencesShowCityToGuestUsers]
      ,[UserPreferencesShowStateToGuestUsers]
      ,[UserPreferencesShowPostalCodeToGuestUsers]
      ,[UserPreferencesShowCountryToGuestUsers]
      ,[UserPreferencesDisableFeedbackEmail]
      ,[UserPreferencesDisableWorkEmail]
      ,[UserPreferencesHideS1BrowserUI]
      ,[UserPreferencesDisableEndorsementEmail]
      ,[UserPreferencesPathAssistantCollapsed]
      ,[UserPreferencesCacheDiagnostics]
      ,[UserPreferencesShowEmailToGuestUsers]
      ,[UserPreferencesShowManagerToGuestUsers]
      ,[UserPreferencesShowWorkPhoneToGuestUsers]
      ,[UserPreferencesShowMobilePhoneToGuestUsers]
      ,[UserPreferencesShowFaxToGuestUsers]
      ,[UserPreferencesShowStreetAddressToGuestUsers]
      ,[UserPreferencesLightningExperiencePreferred]
      ,[UserPreferencesHideEndUserOnboardingAssistantModal]
      ,[UserPreferencesHideLightningMigrationModal]
      ,[UserPreferencesHideSfxWelcomeMat]
      ,[UserPreferencesHideBiggerPhotoCallout]
      ,[UserPreferencesGlobalNavBarWTShown]
      ,[UserPreferencesGlobalNavGridMenuWTShown]
      ,[UserPreferencesCreateLEXAppsWTShown]
      ,[UserPreferencesFavoritesWTShown]
      ,[ContactId]
      ,[AccountId]
      ,[CallCenterId]
      ,[Extension]
      ,[FederationIdentifier]
      ,[AboutMe]
      ,[FullPhotoUrl]
      ,[SmallPhotoUrl]
      ,[MediumPhotoUrl]
      ,[DigestFrequency]
      ,[DefaultGroupNotificationFrequency]
      ,[JigsawImportLimitOverride]
      ,[LastViewedDate]
      ,[LastReferencedDate]
      ,[BannerPhotoUrl]
      ,[SmallBannerPhotoUrl]
      ,[MediumBannerPhotoUrl]
      ,[IsProfilePhotoActive]
      ,[Business_Unit__c]
      ,[Country_Location__c]
      ,[Reporting_Region__c]
      ,[RGIS_International_User__c]
      ,[qbdialer__InsideSales_Admin__c]
      ,[qbdialer__is_subdomain__c]
      ,[qbdialer__is_token__c]
      ,[qbdialer__password__c]
      ,[qbdialer__permissions__c]
      ,[qbdialer__username__c]
      ,[Profile_Name__c]
      ,[Edit_Link__c]
  FROM [Salesforce_ODS].[dbo].[SF_User] U
  left join user_country uc on u.name = uc.full_name


GO

/****** Object:  View [dbo].[vw_Opportunities]    Script Date: 24/11/2017 14:21:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create view [dbo].[vw_Opportunities] AS 
SELECT [Id]
      ,[IsDeleted]
      ,[AccountId]
	  ,(Select Name from Account A where A.Id = O.AccountId) As AccountName
      ,[RecordTypeId]
      ,[Name]
      ,[Description]
      ,[StageName]
      ,[Amount]
      ,[Probability]
      ,[CloseDate]
      ,[Type]
      ,[NextStep]
      ,[LeadSource]
      ,[IsClosed]
      ,[IsWon]
      ,[ForecastCategory]
      ,[ForecastCategoryName]
      ,[CampaignId]
	  ,(Select Name from Campaign C where C.Id = O.CampaignId) As CampaignName
      ,[HasOpportunityLineItem]
      ,[Pricebook2Id]
      ,[OwnerId]
	  ,(Select Name from vw_User U Where U.Id = O.OwnerId) As OwnerName
	  ,(Select UserRoleName from vw_User U Where U.Id = O.OwnerId) As OwnerRoleName
	  ,(Select Country from vw_User U Where U.Id = O.OwnerId) As OwnerCountry
      ,[CreatedDate]
      ,[CreatedById]
	  ,(Select Name from vw_User U Where U.Id = O.CreatedById) As CreatedByName
	  ,(Select UserRoleName from vw_User U Where U.Id = O.CreatedById) As CreatedByRoleName
	  ,(Select Country from vw_User U Where U.Id = O.CreatedById) As CreatedByCountry
      ,[LastModifiedDate]
      ,[LastModifiedById]
      ,[SystemModstamp]
      ,[LastActivityDate]
      ,[FiscalQuarter]
      ,[FiscalYear]
      ,[Fiscal]
      ,[LastViewedDate]
      ,[LastReferencedDate]
      ,[HasOpenActivity]
      ,[HasOverdueTask]
      ,[Reason_Won_Lost__c]
      ,[Won_Lost_Description__c]
      ,[Won_From_Lost_To__c]
      ,[Oracle_Customer_Number__c]
      ,[Countries_of_Service__c]
      ,[Contract_Begin_Date__c]
      ,[Contract_End_Date__c]
      ,[WonFrom_LostTo__c]
      ,[Associated_Blue_Sheet__c]
      ,[Services__c]
      ,[Division__c]
      ,[First_Date_of_Service__c]
      ,[Sold_By__c]
      ,[District__c]
      ,[Rate__c]
      ,[Last_District_Update__c]
      ,[Market_Segment__c]
      ,[Internal_Referral_Source__c]
      ,[First_Service_Date__c]
      ,[Store_Type__c]
      ,[Frequency__c]
      ,[Other__c]
      ,[Expected_2010_Revenue__c]
      ,[ACV__c]
      ,[TCV__c]
      ,[Not_Interested__c]
      ,[Next_Step__c]
      ,[Opportunity_Owner_Is_Active__c]
      ,[Turn_Off_Opportunity_Workflows__c]
      ,[Proposal_Created_Timestamp__c]
      ,[Closed_Won_Timestamp__c]
      ,[FSCLeadOwnerId__c]
      ,[FSC_Lead_Owner__c]
  FROM [Salesforce_ODS].[dbo].[Opportunity] O
GO

/****** Object:  View [dbo].[vw_EU_Pipeline_Login_Activity]    Script Date: 24/11/2017 14:21:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE view [dbo].[vw_EU_Pipeline_Login_Activity] AS

Select FirstName
,LastName
,UserRoleName
,LastLoginDate
,Name
,Country
  FROM [Salesforce_ODS].[dbo].[vw_User]
  where country  in  ('ROI','Italy','Germany','Hungary','UK','Ireland','Spain','Romania')
  and isActive = 1


GO

/****** Object:  View [dbo].[vw_EU_Pipeline_Europe]    Script Date: 24/11/2017 14:21:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE view [dbo].[vw_EU_Pipeline_Europe] AS
SELECT OwnerName as Opportunity_Owner
,OwnerRoleName as Owner_Role
,Name as Opportunity_Name
,[Type]
,[LeadSource] as Lead_Source
,convert(integer,round([Expected_2010_Revenue__c],0)) As Current_FY_Expected_Revenue
,convert(integer,round(Amount,0)) as Amount
,closeDate as Close_Date
,[Next_Step__c] as Next_Step
,StageName as Stage
,Probability
,[CreatedDate] as Created_Date
,CreatedByName as Created_By
,CreatedByRoleName as Created_By_Role_Name
,[AccountName] as Account_Name
,[Reason_Won_Lost__c] as Reason_Won_Lost
,[CampaignName] as Campaign_Name
,CASE CampaignName WHEN 'Portugal Campaign' Then 'Portugal' ELSE [OwnerCountry] END as Owner_Country
,CreatedByCountry as Created_By_Country
,case ownercountry when 'Spain' then 'Iberia' else Ownercountry end as country_group

  FROM [Salesforce_ODS].[dbo].[vw_Opportunities]

  Where (OwnerCountry in ('ROI','Italy','Germany','Hungary','UK','Ireland','Spain','Romania')
 OR CreatedByCountry in  ('ROI','Italy','Germany','Hungary','UK','Ireland','Spain','Romania'))
  and NOT (stageName in ('Won','Lost') and closedate < DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()), 0))


 






GO

/****** Object:  View [dbo].[vw_EU_Pipeline_Scorecard_Created]    Script Date: 24/11/2017 14:21:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE view [dbo].[vw_EU_Pipeline_Scorecard_Created] AS
Select 
Created_By_Country
,Created_By
,Created_By_Role_Name
,Total_Opps
,Opps_In_Progress
,left(((convert(Decimal(18,2),Opps_In_Progress)/Total_Opps)*100),5) as Opps_Perc
,Opps_Won
,left(((convert(Decimal(18,2),Opps_Won)/Total_Opps)*100),5) as Won_Perc
,Amount_Won
,Opps_Lost
,left(((convert(Decimal(18,2),Opps_Lost)/Total_Opps)*100),5) as Lost_Perc
From (


SELECT Created_By_Country
,Created_By
,Created_By_Role_Name
,count(opportunity_Name) as Total_Opps
,Sum(CASE Stage WHEN 'Lost' THEN 0 WHEN 'Won' THEN 0 ELSE 1 END) as Opps_In_Progress
,Sum(CASE Stage WHEN 'Lost' THEN 0 WHEN 'Won' THEN 1 ELSE 0 END) as Opps_Won
,Sum(CASE Stage WHEN 'Lost' THEN 0 WHEN 'Won' THEN Amount ELSE 0 END) as Amount_Won
,Sum(CASE Stage WHEN 'Lost' THEN 1 WHEN 'Won' THEN 0 ELSE 0 END) as Opps_Lost
FROM [Salesforce_ODS].[dbo].[vw_EU_Pipeline_Europe] E
group by Created_By_Country, Created_By, Created_by_role_Name) Opps


GO

/****** Object:  View [dbo].[vw_EU_Pipeline_Scorecard_Owner]    Script Date: 24/11/2017 14:21:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE view [dbo].[vw_EU_Pipeline_Scorecard_Owner] AS
Select 
Owner_Country
,Opportunity_Owner
,Owner_Role
,Total_Opps
,Opps_In_Progress
,left(((convert(Decimal(18,2),Opps_In_Progress)/Total_Opps)*100),5) as Opps_Perc
,Opps_Won
,left(((convert(Decimal(18,2),Opps_Won)/Total_Opps)*100),5) as Won_Perc
,Amount_Won
,Opps_Lost
,left(((convert(Decimal(18,2),Opps_Lost)/Total_Opps)*100),5) as Lost_Perc
From (


SELECT Owner_Country
,Opportunity_Owner
,Owner_Role
,count(opportunity_Name) as Total_Opps
,Sum(CASE Stage WHEN 'Lost' THEN 0 WHEN 'Won' THEN 0 ELSE 1 END) as Opps_In_Progress
,Sum(CASE Stage WHEN 'Lost' THEN 0 WHEN 'Won' THEN 1 ELSE 0 END) as Opps_Won
,Sum(CASE Stage WHEN 'Lost' THEN 0 WHEN 'Won' THEN Amount ELSE 0 END) as Amount_Won
,Sum(CASE Stage WHEN 'Lost' THEN 1 WHEN 'Won' THEN 0 ELSE 0 END) as Opps_Lost
FROM [Salesforce_ODS].[dbo].[vw_EU_Pipeline_Europe] E
group by  Owner_Country
,Opportunity_Owner
,Owner_Role) Opps


GO

/****** Object:  View [dbo].[vw_EU_Pipeline_Scorecard_Report_Owner]    Script Date: 24/11/2017 14:21:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE view [dbo].[vw_EU_Pipeline_Scorecard_Report_Owner] as

select  
[Owner_Country]
      ,[Opportunity_Owner]
      ,[Owner_Role]
      ,[Total_Opps]
      ,[Opps_In_Progress]
      ,[Opps_Perc]
      ,[Opps_Won]
      ,[Won_Perc]
      ,[Amount_Won]
      ,[Opps_Lost]
      ,[Lost_Perc]
from (
SELECT [Owner_Country]
      ,[Opportunity_Owner]
      ,[Owner_Role]
      ,[Total_Opps]
      ,[Opps_In_Progress]
      ,[Opps_Perc]
      ,[Opps_Won]
      ,[Won_Perc]
      ,[Amount_Won]
      ,[Opps_Lost]
      ,[Lost_Perc]
  FROM [Salesforce_ODS].[dbo].[vw_EU_Pipeline_Scorecard_Owner]
     where Opportunity_Owner in (select name from vw_EU_Pipeline_Login_Activity) and [Opps_In_Progress] > 0
  
  union
SELECT'', 'TOTAL',''
      ,SUM([Total_Opps]) as Total_Opps
      ,sum([Opps_In_Progress]) as Opps_In_Progress
      ,left(sum([Opps_In_Progress])/SUM(convert(decimal(18,2),[Total_Opps]))*100,5)
      ,SUM([Opps_Won]) as Opps_Won
      ,left(SUM([Opps_Won])/SUM(convert(decimal(18,2),[Total_Opps]))*100,5) 
      ,sum([Amount_Won]) as Amount_Won
      ,sum([Opps_Lost]) as total_Lost
      ,left(sum([Opps_Lost])/SUM(convert(decimal(18,2),[Total_Opps]))*100,5)
  FROM [Salesforce_ODS].[dbo].[vw_EU_Pipeline_Scorecard_owner]
     where Opportunity_Owner in (select name from vw_EU_Pipeline_Login_Activity) and [Opps_In_Progress] > 0

  ) a 

GO

/****** Object:  View [dbo].[vw_EU_Pipeline_Scorecard_Report_Creator]    Script Date: 24/11/2017 14:21:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[vw_EU_Pipeline_Scorecard_Report_Creator] as

select  
[Created_By_Country]
      ,[Created_By]
      ,[Created_By_Role_Name]
      ,[Total_Opps]
      ,[Opps_In_Progress]
      ,[Opps_Perc]
      ,[Opps_Won]
      ,[Won_Perc]
      ,[Amount_Won]
      ,[Opps_Lost]
      ,[Lost_Perc]
from (
SELECT TOP 1000 [Created_By_Country]
      ,[Created_By]
      ,[Created_By_Role_Name]
      ,[Total_Opps]
      ,[Opps_In_Progress]
      ,[Opps_Perc]
      ,[Opps_Won]
      ,[Won_Perc]
      ,[Amount_Won]
      ,[Opps_Lost]
      ,[Lost_Perc]
  FROM [Salesforce_ODS].[dbo].[vw_EU_Pipeline_Scorecard_Created]
    where created_by in (select name from vw_EU_Pipeline_Login_Activity) and Created_By_Role_Name like '%Sales Coordinator%'
  
  union
SELECT'', 'TOTAL',''
      ,SUM([Total_Opps]) as Total_Opps
      ,sum([Opps_In_Progress]) as Opps_In_Progress
      ,left(sum([Opps_In_Progress])/SUM(convert(decimal(18,2),[Total_Opps]))*100,5)
      ,SUM([Opps_Won]) as Opps_Won
      ,left(SUM([Opps_Won])/SUM(convert(decimal(18,2),[Total_Opps]))*100,5) 
      ,sum([Amount_Won]) as Amount_Won
      ,sum([Opps_Lost]) as total_Lost
      ,left(sum([Opps_Lost])/SUM(convert(decimal(18,2),[Total_Opps]))*100,5)
  FROM [Salesforce_ODS].[dbo].[vw_EU_Pipeline_Scorecard_Created]
     where created_by in (select name from vw_EU_Pipeline_Login_Activity) and Created_By_Role_Name like '%Sales Coordinator%'

  ) a 



GO

/****** Object:  View [dbo].[vw_EU_Pipeline_Scorecard_Report]    Script Date: 24/11/2017 14:21:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create view [dbo].[vw_EU_Pipeline_Scorecard_Report] as
select * from (
  select -1 as ord,'' AS COUNTRY,'' AS ROLE,'CONVERSION MEASURED BY OPPS CREATED BY' AS NAME,'' AS TOTAL_OPPS,'' AS OPPS_IP,'' AS OPPS_PERC,'' AS WON,'' AS WON_PERC,'' AS WON_AMOUNT,'' AS LOST,'' AS LOST_PERC
  UNION
  select 0,'COUNTRY','ROLE','NAME','TOTAL OPPS','OPPS IN PROGRESS','OPPS %','WON','WON %','WON �','LOST','LOST %'
  union
SELECT CASE created_by when 'TOTAL' then 2 else 1 end as ord,
 [Created_By_Country]
      ,[Created_By]
      ,[Created_By_Role_Name]
      ,convert(varchar(1000),[Total_Opps])
      ,convert(varchar(1000),[Opps_In_Progress])
      ,[Opps_Perc]+'%'
      ,convert(varchar(1000),[Opps_Won])
      ,[Won_Perc]+'%'
      ,convert(varchar(1000),[Amount_Won])
      ,convert(varchar(1000),[Opps_Lost])
      ,[Lost_Perc]+'%'
  FROM [Salesforce_ODS].[dbo].[vw_EU_Pipeline_Scorecard_Report_Creator]
  union
  select 4,'','','','','','','','','','',''
  union
  select 5,'','','CONVERSION MEASURED BY OPPS BY OWNER','','','','','','','',''
  union
  select 6,'COUNTRY','ROLE','NAME','TOTAL OPPS','OPPS IN PROGRESS','OPPS %','WON','WON %','WON �','LOST','LOST %'
  union
  SELECT CASE Opportunity_owner when 'TOTAL' then 8 else 7 end as ord,
  [Owner_Country]
      ,[Opportunity_Owner]
      ,[Owner_Role]
      ,convert(varchar(1000),[Total_Opps])
      ,convert(varchar(1000),[Opps_In_Progress])
      ,[Opps_Perc]+'%'
      ,convert(varchar(1000),[Opps_Won])
      ,[Won_Perc]+'%'
      ,convert(varchar(1000),[Amount_Won])
      ,convert(varchar(1000),[Opps_Lost])
      ,[Lost_Perc]+'%'
  FROM [Salesforce_ODS].[dbo].[vw_EU_Pipeline_Scorecard_Report_Owner]
) a
GO

/****** Object:  View [dbo].[vw_Expected_Revenue]    Script Date: 24/11/2017 14:21:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_Expected_Revenue] as
With Prob as (Select	5	 as probability union 
Select	10	 as probability union 
Select	15	 as probability union 
Select	20	 as probability union 
Select	25	 as probability union 
Select	30	 as probability union 
Select	35	 as probability union 
Select	40	 as probability union 
Select	45	 as probability union 
Select	50	 as probability union 
Select	55	 as probability union 
Select	60	 as probability union 
Select	65	 as probability union 
Select	70	 as probability union 
Select	75	 as probability union 
Select	80	 as probability union 
Select	85	 as probability union 
Select	90	 as probability union 
Select	95	 as probability union 
Select	100	 as probability  )

, CProb AS (SELECT distinct [Owner_Country], prob.probability, stage
FROM [Salesforce_ODS].[dbo].[vw_EU_Pipeline_Europe] join prob on 1=1)


select * from (
select cprob.Owner_Country, CProb.probability, cprob.Stage, isnull(amount,0) as Amount from cprob left join (
SELECT [Owner_Country]
,stage
,Probability
,sum([Current_FY_Expected_Revenue]) as Amount

FROM [vw_EU_Pipeline_Europe] E 

where stage in ('Contract Negotiation','Needs Analysis','Proof of Concept','Proposal')
and [Current_FY_Expected_Revenue] is not null and  [Current_FY_Expected_Revenue] <> 0
group by e.owner_country, e.stage,probability) a on cprob.probability = a.Probability and cprob.Owner_Country = a.Owner_Country and cprob.stage = a.stage
) src PIVOT
(SUM(Amount)
for probability in ( [5],	[10],	[15],	[20],	[25],	[30],	[35],	[40],	[45],	[50],	[55],	[60],	[65],	[70],	[75],	[80],	[85],	[90],	[95],	[100]))piv


GO

/****** Object:  View [dbo].[vw_EU_Pipeline_Expected_Revenue]    Script Date: 24/11/2017 14:21:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[vw_EU_Pipeline_Expected_Revenue] as
SELECT TOP 1000 [Owner_Country]
      ,[Stage]
      ,[5]
      ,[10]
      ,[15]
      ,[20]
      ,[25]
      ,[30]
      ,[35]
      ,[40]
      ,[45]
      ,[50]
      ,[55]
      ,[60]
      ,[65]
      ,[70]
      ,[75]
      ,[80]
      ,[85]
      ,[90]
      ,[95]
      ,[100]
  FROM [Salesforce_ODS].[dbo].[vw_Expected_Revenue]   where stage in ('Contract Negotiation','Needs Analysis','Proof of Concept','Proposal')
  union
  SELECT 'Europe' as Country
      ,[Stage]
      ,SUM([5])
      ,SUM([10])
      ,SUM([15])
      ,SUM([20])
      ,SUM([25])
      ,SUM([30])
      ,SUM([35])
      ,SUM([40])
      ,SUM([45])
      ,SUM([50])
      ,SUM([55])
      ,SUM([60])
      ,SUM([65])
      ,SUM([70])
      ,SUM([75])
      ,SUM([80])
      ,SUM([85])
      ,SUM([90])
      ,SUM([95])
      ,SUM([100])
  FROM [Salesforce_ODS].[dbo].[vw_Expected_Revenue]
  where stage in ('Contract Negotiation','Needs Analysis','Proof of Concept','Proposal')
  group by stage
GO

/****** Object:  View [dbo].[vw_Email_Summary]    Script Date: 24/11/2017 14:21:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE view [dbo].[vw_Email_Summary] AS
SELECT 'Won Amount' as Stage, owner_country as Country,'' as Opportunity_Name, convert(integer,sum(amount)) as Amount
FROM [Salesforce_ODS].[dbo].[vw_EU_Pipeline_Europe]
where stage = 'won'
and owner_country in ('UK', 'Ireland', 'Spain', 'Portugal')
group by owner_country

UNION
SELECT 'Active', owner_country, '', convert(integer,sum(amount)) as Amount
FROM [Salesforce_ODS].[dbo].[vw_EU_Pipeline_Europe]
where stage in  ('Contract Negotiation','Long Term','Needs Analysis','Proof of Concept', 'Proposal')
and owner_country in ('UK', 'Ireland', 'Spain', 'Portugal')
group by owner_country
UNION
SELECT 'Active Effective', owner_country, '', convert(integer,sum(amount)) as Amount
FROM [Salesforce_ODS].[dbo].[vw_EU_Pipeline_Europe]
where stage in  ('Contract Negotiation','Needs Analysis','Proof of Concept','Proposal' )
and owner_country in ('UK', 'Ireland', 'Spain', 'Portugal')
group by owner_country

union

SELECT 'Expected Revenue', [Owner_Country],''
,round(sum([Current_FY_Expected_Revenue] * (Probability/100)),0) as Amount
FROM [vw_EU_Pipeline_Europe] E 
where stage in ('Contract Negotiation','Needs Analysis','Proof of Concept','Proposal')
and owner_country in ('UK', 'Ireland', 'Spain', 'Portugal')
and [Current_FY_Expected_Revenue] is not null and  [Current_FY_Expected_Revenue] <> 0
group by e.owner_country

union
SELECT Stage,Owner_Country,  Opportunity_Name, convert(integer,Amount) as Amount
  FROM [Salesforce_ODS].[dbo].[vw_EU_Pipeline_Europe]
  Where close_date between convert(date,dateadd(dd,-7,getdate())) and convert(date,dateadd(dd,-1,getdate()))
  and owner_country in ('UK', 'Ireland', 'Spain', 'Portugal')
  and stage in ('Won','Lost')

GO

/****** Object:  View [dbo].[vw_Opportunity_Compliance_By_Owner]    Script Date: 24/11/2017 14:21:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create view [dbo].[vw_Opportunity_Compliance_By_Owner] AS
SELECT [Opportunity_Owner]
	  ,LA.Country
	  ,Compliance_Check_Date
      ,SUM([Initial_Valid]) as Total_Initial_Compliance
      ,SUM([Date_Compliance]) as Total_Date_Compliance
      ,SUM([Close_Date_Check]) as Total_Close_Date_Compliance
      ,SUM([Active_Close_Date_Check]) as Total_Active_Close_Date_Compliance
      ,SUM([Reason_Lost_Check]) as Total_Reason_Lost_Compliance
      ,SUM([Future_compliance]) as Total_FollowUp_Date_Compliance
	  ,SUM(CASE WHEN Opportunity_Score = Max_Score THEN 1 ELSE 0 END) as Number_of_Full_Compliance
	  ,COUNT(Opportunity_Owner) as Opportunity_Count
      ,AVG([Opportunity_Compliance]) as Average_Compliance

  FROM [Salesforce_ODS].[dbo].[Opportunity_Compliance] OC join vw_EU_Pipeline_Login_Activity LA on OC.Opportunity_Owner = LA.Name
  group by opportunity_Owner,LA.Country, Compliance_Check_Date

GO



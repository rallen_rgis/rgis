USE [Salesforce_ODS]
GO

/****** Object:  StoredProcedure [dbo].[usp_SalesForce_Compliance]    Script Date: 24/11/2017 14:21:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[usp_SalesForce_Compliance] as 
SET DATEFORMAT dmy; 
With intials AS (
SELECT distinct isnull(left([FirstName],1),'')+isnull(left([LastName],1),'') inits FROM [Salesforce_ODS].[dbo].[vw_User]
union SELECT 'CZ'  union  SELECT 'FC'    union  SELECT 'MP'    union  SELECT 'VB'    union  SELECT 'CH'    union  SELECT 'PDF'    union  SELECT 'WST'
union SELECT 'PF' )

Select LA.Name, LA.UserRoleName
, isnull([Won/Lost in future], 0) AS 'Won/Lost in future'
, isnull([Active invalid Close Date], 0) as 'Invalid Close Dates'
, isnull([Invalid Won/Lost], 0) as 'Invalid Won/Lost Reason'
, isnull(nsi.[Next Step Initials], 0) as 'Invalid initials in Next Step'
, isnull(nsd.[Next Step Date], 0) as 'Invalid date in Next Step'

, isnull([Won/Lost in future], 0) + isnull([Active invalid Close Date], 0) + isnull([Invalid Won/Lost], 0)+ isnull(nsi.[Next Step Initials], 0) + isnull(nsd.[Next Step Date], 0) as 'Total Non Compliant'
,(select count(*) from vw_EU_Pipeline_Europe E where   E.Opportunity_Owner = LA.Name) as Total_Opps


from [dbo].[vw_EU_Pipeline_Login_Activity] LA LEFT JOIN 

(select opportunity_owner, count(next_step_date) as 'Next Step Date' FROm (



select Opportunity_Owner,next_step_date, CASE  WHEN Date_Compliance= 1 THEN convert(date,replace(replace(replace(next_step_date,',',''),'.','/'),'-','/')) END AS NSD
,date_compliance,
CASE  WHEN Date_Compliance= 1 THEN
	CASE WHEN convert(date,replace(replace(replace(next_step_date,',',''),'.','/'),'-','/')) > dateadd(dd,-2,getdate()) then 1 Else 0 END
	END  as future_compliance

from (
SELECT Opportunity_Owner, ltrim(right(next_step, charindex(' ',reverse(next_step)))) next_step_date ,
isdate(replace(ltrim(right(next_step, charindex(' ',reverse(next_step)))),',','')) as Date_Compliance
  FROM [Salesforce_ODS].[dbo].[vw_EU_Pipeline_Europe]
  Where stage not in ('Won','Lost') ) a 
 ) n
  where date_compliance = 0 or isnull(future_compliance,0) = 0
  group by Opportunity_Owner) nsd


  -- (initials,past date, past action, follow up action follow up date) 
  ON LA.Name = nsd.Opportunity_Owner
  LEFT JOIN

  (select opportunity_owner, count(next_step) as 'Next Step Initials'
from (
SELECT distinct Opportunity_Owner, next_step
  FROM [Salesforce_ODS].[dbo].[vw_EU_Pipeline_Europe]
  where replace(replace(left(next_step,4),'(',''),')','') not in ( select inits from intials) --First Pass
  ) a  WHERE left(next_step,2) not in ( select inits from intials) --Second Pass
group by Opportunity_Owner) nsi

on LA.Name = nsi.Opportunity_Owner 

LEFT JOIN
(SELECT [Opportunity_Owner]
,count(stage) as 'Won/Lost in future'
FROM [Salesforce_ODS].[dbo].[vw_EU_Pipeline_Europe]
where stage in ('Won','Lost') and Close_Date > getdate()
group by Opportunity_Owner ) WLF
ON LA.Name = WLF.Opportunity_Owner


LEFT JOIN
(SELECT [Opportunity_Owner]
,count(stage) as 'Active invalid Close Date'

FROM [Salesforce_ODS].[dbo].[vw_EU_Pipeline_Europe]
where stage not in ('Won','Lost') and Close_Date < dateadd(dd,-2,getdate()) --2 days grace period
group by Opportunity_Owner) AICD
ON LA.Name = AICD.Opportunity_Owner


LEFT JOIN
(SELECT [Opportunity_Owner]
,count(stage) as 'Invalid Won/Lost'
FROM [Salesforce_ODS].[dbo].[vw_EU_Pipeline_Europe]
where stage in ('Lost') and Reason_Won_Lost is null
group by Opportunity_Owner) IWL
ON LA.Name = IWL.Opportunity_Owner
ORDER by 8 desc
GO

/****** Object:  StoredProcedure [dbo].[usp_SalesForce_Compliance_Details]    Script Date: 24/11/2017 14:21:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE procedure [dbo].[usp_SalesForce_Compliance_Details] AS 
SET DATEFORMAT dmy; 
With intials AS (
SELECT distinct isnull(left([FirstName],1),'')+isnull(left([LastName],1),'') inits FROM [Salesforce_ODS].[dbo].[vw_User]
union SELECT 'CZ'  union  SELECT 'FC'    union  SELECT 'MP'    union  SELECT 'VB'    union  SELECT 'CH'    union  SELECT 'PDF'    union  SELECT 'WST'
union SELECT 'PF' )
,Compliance AS (
SELECT [Opportunity_Owner]
,stage
,[Opportunity_Name]
,next_step
,replace(replace(replace(ltrim(right(next_step, charindex(' ',reverse(next_step)))),',',''),'.','/'),'-','/') As Next_Step_Date
,Close_date
,reason_won_lost
,CASE 
WHEN replace(replace(left(next_step,4),'(',''),')','') in ( select inits from intials) THEN 1 
WHEN left(next_step,2) in  ( select inits from intials) THEN 1 
ELSE 0 END as Initial_Valid

,CASE WHEN LEN(replace(replace(replace(ltrim(right(next_step, charindex(' ',reverse(next_step)))),',',''),'.','/'),'-','/')) <5 THEN 0 
ELSE isdate(replace(replace(replace(ltrim(right(next_step, charindex(' ',reverse(next_step)))),',',''),'.','/'),'-','/')) END as Date_Compliance



,CASE WHEN Stage		in ('Won','Lost')	THEN CASE WHEN Close_Date < getdate() then 1 else 0 END else 0 END AS Close_Date_Check
,CASE WHEN Stage not	in ('Won','Lost')	THEN CASE WHEN Close_Date <  dateadd(dd,-2,getdate()) then 0 else 1 END else 0 END AS Active_Close_Date_Check

,CASE WHEN Stage		in ('Lost')			THEN CASE WHEN len(isnull(reason_won_lost,0)) > 1 then 1 else 0 END else 0 END AS Reason_Lost_Check
FROM [Salesforce_ODS].[dbo].[vw_EU_Pipeline_Europe] M)

, Stage_Score AS (
Select'Lost' AS Stage,					4 AS Score union
Select'Stalled' AS Stage,				4 AS Score union
Select'Needs Analysis' AS Stage,		4 AS Score union
Select'Proposal' AS Stage,				4 AS Score union
Select'Contract Negotiation' AS Stage,	4 AS Score union
Select'Won' AS Stage,					3 AS Score union
Select'Proof of Concept' AS Stage,		4 AS Score union
Select'Long Term' AS Stage,				4 AS Score )


, Summary AS (
Select C.* 
,CASE  WHEN Date_Compliance = 1 THEN
CASE WHEN convert(date,Next_Step_Date) > dateadd(dd,-2,getdate()) 
then 1 Else 0 END ELSE 0 END  as Future_compliance
,CASE WHEN c.STAGE in ('Won')	THEN initial_valid + Close_Date_Check + Date_Compliance
WHEN		c.STAGE in ('Lost') THEN initial_valid + Close_Date_Check + Date_Compliance + Reason_Lost_Check
ELSE								 Initial_valid + Date_Compliance  +  Active_Close_Date_Check 
+ CASE  WHEN Date_Compliance = 1 THEN CASE WHEN convert(date,Next_Step_Date) > dateadd(dd,-2,getdate()) then 1 Else 0 END ELSE 0 END --Future_Check


END as Opportunity_Score ,SS.Score as Max_Score

From Compliance C join Stage_Score SS on C.Stage = SS.Stage)

INSERT into Opportunity_Compliance
Select *, left(convert(decimal(18,2),Opportunity_Score)/Max_Score,4) As Opportunity_Compliance, getdate() as Compliance_Check_Date  from Summary order by 1,2

Truncate Table Compliance_Summary_L5W
INSERT INTO Compliance_Summary_L5W
select *  from (
SELECT [Opportunity_Owner]      ,[Country]	  ,[Average_Compliance]	  ,b.Row_Num
FROM [Salesforce_ODS].[dbo].[vw_Opportunity_Compliance_By_Owner] c join (
Select Compliance_Check_Date, ROW_NUMBER() OVER (ORDER BY Compliance_Check_Date desc) as Row_Num from (
Select distinct convert(date,compliance_Check_Date) as Compliance_Check_Date from [vw_Opportunity_Compliance_By_Owner] ) a )b on
convert(date,c.[Compliance_Check_Date]) = b.Compliance_Check_Date ) SRC PIVOT (AVG(Average_Compliance) for row_num in ([1],[2],[3],[4],[5]))piv








GO


